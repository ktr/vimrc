" we assume you will clone this repo to ~/.config/vim

" --------------------------------------------------------------------------
" windows/*nix specific configuration
" --------------------------------------------------------------------------

" should create the following directories before launching vim (otherwise, vim
" will error out as it won't be able to load swap files, etc.).

" complements : http://items.sjbach.com/319/configuring-vim-right
set backupdir=~/.vim-tmp/bak
set directory=~/.vim-tmp/bak

" http://stackoverflow.com/a/15660810/277924
set undodir=~/.vim-tmp/undo,.
set undofile

" set pythonthreedll=H:\bin\python\python37.dll
" nmap gc :!start C:\Windows\system32\cmd.exe<CR>

" http://stackoverflow.com/a/6286925
set viminfo+=n~/.vim/viminfo

let g:ale_python_pylint_options = '--rcfile=~/.config/pylintrc'

" --------------------------------------------------------------------------
" end windows/*nix specific configuration
" --------------------------------------------------------------------------

set nocompatible
filetype off

" rtp = runtimepath
set rtp+=~/.config/vim/bundle/vim-buftabline.vim
set rtp+=~/.config/vim/bundle/ctrlp.vim
set rtp+=~/.config/vim/bundle/lightline.vim
set rtp+=~/.config/vim/bundle/ale
set rtp+=~/.config/vim/bundle/lightline-ale

set rtp+=~/.config/vim

" Plugin 'https://github.com/davidhalter/jedi-vim.git'
" Plugin 'https://github.com/ervandew/supertab'
" Plugin 'https://github.com/tpope/vim-fugitive.git'

" Plugin 'https://github.com/Vimjas/vim-python-pep8-indent'
" Plugin 'https://github.com/cjrh/vim-conda'

" Plugin 'https://github.com/godlygeek/tabular'
" Plugin 'https://github.com/plasticboy/vim-markdown'
" Plugin 'https://github.com/vimwiki/vimwiki'
" Plugin 'https://github.com/vim-scripts/closetag.vim'

filetype plugin indent on

set laststatus=2

set fileformat=unix
set fileformats=unix,dos
set encoding=utf-8

" let g:lightline = { 'colorscheme': 'wombat', }

" colorscheme wombat " 2011-Aug-17 @ 3:15:02 PM
colorscheme smyck  " 2012-Aug-02 @ 8:21:10 AM ; 2017-Sep-14 @ 10:07:43 PM
" colorscheme hybrid " 2013-Dec-23 @ 9:35:22 PM
" colorscheme tigrana-256-dark " NA
" colorscheme carbonized-dark  " NA

" set go=gmrLtT " gui options
set guioptions=m " menu only
set nowrap

" case insensitive search by default
set ic
" unless there is an uppercase character - then keep it case sensitive
set scs

set guifont=Consolas:h11:cANSI

set mousehide       " Hide the mouse when typing text
set shiftwidth=4
set softtabstop=4
set nobackup
set writebackup
set expandtab
set diffopt=vertical

" Use Ctrl-Tab, Ctrl-Shft-Tab to switch between windows
nnoremap <C-Tab>    :bnext<CR>
nnoremap <S-C-Tab>  :bprevious<CR>
nnoremap gf <C-w>gf
nnoremap gF <C-w>gF

" Added on 2007-Dec-08 @ 10:13:32 PM ... sort of like emacs
cmap <C-a> <Home>
imap <C-a> <Home>
imap <C-e> <End>

" easier code completion
inoremap <C-Space> <C-x><C-o>

" Added on 2008-Jan-29 @ 2:04:26 PM: use +/- for folding/unfolding
" (respectively) rather than the -somewhat- unintuitive zo/zc
map + zo
map - zc

function SqlStrip()
  %left               " remove all spaces at beginning of lines
  %s/--.*//           " remove commas
  %s/\n/ /            " turn newlines into spaces
  s/^ //              " remove extra space at beginning of line
  s/ $//              " ... and at the end of the line
  let @*=getline('.') " copy to clipboard
  undo                " reset to how we started
endfunction

" ----------- # All of our F-? keys mapped to make things easier # ----------- "
"
" F1  = toggle Tagbar window on/off
" F2  = List of buffers (change by choosing number)
" F3  = toggle ale window on/off (linter / syntax checker)
" F4  = remove newlines, strip extraneous whitespace, and copy to clipboard
" F5  = change to the current directory
" F6  = fix lines with extra spaces at the end
" F7  = turn numbers on/off
" F8  = erroneous ^M tag
" F9  = turn spelling on or off (ie, toggle it)
" F10 = copy <motion> to clipboard
" F11 = copy remainder of file to clipboard
" F12 = unassigned
map <silent> <F1> :TagbarToggle <Enter>
map <F2> :buffers<CR>:buffer<Space>
map <silent> <F3> :ALEToggle <Enter>
" F4  = switches between 2 windows with one keystroke
" map <F4> <C-w>w
map <F4> :call SqlStrip()<CR>
map <silent> <F5> :cd %:p:h <Enter>:pwd <Enter>
map <silent> <F6> :%s:[ \t]\+$::g <Enter> :noh <Enter>
map <silent> <F7> :set number!<Enter>
map <silent> <F8> :%s/\r$//g <Enter>
map <silent> <F9> :set spell! <Enter>
map <F10> "*y
map <F11> "*yG

map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>:cs kill 0<CR>:!cscope -R -b<CR>:cs add cscope.out<CR>

" ------------------------- # End F-? key mappings # ------------------------- "

nnoremap ' `
nnoremap ` '

" Abbreviation replacements:
" Enter the current date:
iab NOW <C-R>=strftime("%Y-%b-%d @ %X")<CR>
iab TODAY <C-R>=strftime("%Y-%b-%d")<CR>
iab neqm if __name__ == "__main__"
iab bpt breakpoint()
iab ipy from IPython import embed; embed()
" which one is more memorable?
iab VBA ' vim: set ft=vb :<CR>Option Explicit<CR><CR>Sub Test()<CR>End Sub
iab gml kevin.t.ryan@gmail.com
iab impl import matplotlib.pyplot as plt
iab pL # pylint: disable

augroup filetypedetect
    " autocmd's
    au BufNewFile,Bufread *.txt  setf text
    au BufNewFile,Bufread *.inc setf php
    au BufNewFile,Bufread *.zpl setf zimpl
    au BufNewFile,Bufread *.tt setf html
    au BufNewFile,Bufread *.mako setf mako
    au BufNewFile,Bufread *.clj setf clojure
    " PgSQL
    au BufNewFile,BufRead *.pgsql setf pgsql
    " au Filetype html,xml,xsl,xslt,smarty,htmlcheetah,jsp source ~/.vim/ftplugin/html/HTML.vim
    au Filetype lisp set showmatch
    au FileType htmlcheetah set ai
    au FileType smarty set ai
    au FileType javascript set si
    au FileType scheme set shiftwidth=1
    au FileType ragel set si

    " au FileType c set makeprg=gcc\ -Wall\ -std=c99\ -o\ %:t:r\ %
    " au FileType cpp set makeprg=g++\ %

    au FileType tex set shiftwidth=2
    au FileType tex set softtabstop=2
    au FileType tex set si

    au BufNewFile,BufRead *.r setf r
    au BufNewFile,BufRead *.R setf r
    au BufNewFile,BufRead *.pgsql setf pgsql
    au BufNewFile,BufRead *.rl setf ragel

    " Keep folds
    " au BufWinLeave *.* mkview
    " au BufWinEnter *.* silent loadview

    " Don't show documentation window with jedi-vim
    autocmd FileType python setlocal completeopt-=preview
    autocmd FileType python setlocal omnifunc=python3complete#Complete

    autocmd VimEnter .plan $pu=\"\n\"
    autocmd VimEnter .plan $pu=strftime('%Y-%m-%d %H:%M %p')
    autocmd VimEnter .plan $pu=\"\n\"
augroup END

augroup omnion
    au FileType javascript set omnifunc=javascriptcomplete#CompleteJS
    au FileType html set omnifunc=htmlcomplete#CompleteTags
    au FileType css set omnifunc=csscomplete#CompleteCSS
    au FileType htmlcheetah set omnifunc=htmlcomplete#CompleteTags
augroup END

" Switch on syntax highlighting if it wasn't on yet.
if !exists("syntax_on")
    syntax on
endif

" Switch on search pattern highlighting.
set hlsearch

" Stop the incessant beeping and turn off the hideous screen "flash" as well
" (added on 2007-May-30 @ 12:10:25 AM)
set noerrorbells
set visualbell
set t_vb=

" Like TextMate, wrap selected items with quote, double-quote, etc.
" (inspired by http://programming.reddit.com/info/20w15/comments)
" (added on 2007-Jun-25 @ 3:01:26 PM)
vmap ( s()<Esc>P
vmap [ s[]<Esc>P
vmap { s{}<Esc>P

" highlights the current column
set cursorline

" the following ensures that you must hit ;<Tab> when HTML.vim is on rather than
" just <Tab> to skip to the next section (bc we use <Tab> for autocomplete).
let g:no_html_tab_mapping = 'yes'
" Lowercase tags - html5
let g:html_tag_case = 'lowercase'
let g:no_html_toolbar = 'yes'

set listchars=tab:>-,trail:�,eol:$
nmap <silent> <leader>s :set nolist!<CR>

" see http://dancingpenguinsoflight.com/2009/02/python-and-vim-make-your-own-ide/
" autocmd FileType python set complete+=k~/.vim/syntax/python.vim
" Following lets you type ":make" and find list of errors ... toggle through with :cn and :cp (or :clist)
" autocmd BufRead *.py set makeprg=python\ -c\ \"import\ py_compile,sys;\ sys.stderr=sys.stdout;\ py_compile.compile(r'%')\"
" autocmd BufRead *.py set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m

inoremap <C-v> <ESC>"*pa

noremap <C-l> <C-w>l
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k

set hidden
set incsearch

command! MakeCtags !ctags -R --exclude=build --exclude=reporting-env --languages=python --python-kinds=-iv .

autocmd CursorMovedI *.* if pumvisible() == 0|pclose|endif
autocmd InsertLeave *.* if pumvisible() == 0|pclose|endif
set scrolloff=1
nnoremap <S-F8> :botright cwindow<CR>
nnoremap <C-n> :cn<CR>
nnoremap <C-p> :cp<CR>
set cscopequickfix=s-,c-,d-,i-,t-,e- " use quickfix window for cscope
map <silent> <S-F8> :botright cwindow<CR>

" Copy current file/line no into clipboard (eg, to use w GDB)
" :h filename-modifiers
map ,n <Esc>:let @*=expand("%:t") . ":" . line(".")<CR>:echo "Copied filename/line no to clipboard"<CR>

" Copy current filename (including path) into clipboard
map ,f :let @*=expand("%:p")<CR>:echo "Copied filename (incl. path) to clipboard"<CR>
map ,d :let @*=expand("%:p:h")<CR>:echo "Copied directory to clipboard"<CR>
map ,l :let @*="load -r " . line(".") . " " . expand("%:p")<CR>:echo "Copied filename (incl. path) AND linenumber to clipboard"<CR>

" On windows, use alt-a / alt-x to increment / decrement number under cursor
nnoremap <A-a> <C-a>
nnoremap <A-x> <C-x>

" http://stackoverflow.com/questions/235439/vim-80-column-layout-concerns
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
highlight OverLength ctermbg=red ctermfg=white guibg=#6B1414
match OverLength /\%81v.\+/

" http://bit.ly/2xT8IGE
autocmd FileType python set makeprg=pylint\ --reports=n\ --msg-template=\"{path}:{line}:\ {msg_id}\ {symbol},\ {obj}\ {msg}\"\ %:p
autocmd FileType python set errorformat=%f:%l:\ %m

" http://alols.github.io/2012/11/07/writing-prose-with-vim/
command! -range=% SoftWrap
            \ <line2>put _ |
            \ <line1>,<line2>g/.\+/ .;-/^$/ join |normal $x

" don't change position:
nmap Q gw$

let g:ackprg = 'ag --vimgrep'

" Remove mswin.vim mappings
" nunmap <c-f>
" iunmap <c-f>
" cunmap <c-f>

let g:buftabline_indicators=1 " modified?
let g:buftabline_show=1       " only show when > 1 buffer
let g:buftabline_numbers=1    " show vim buffer number
let g:SuperTabDefaultCompletionType = '<c-n>'
" let g:SuperTabContextDefaultCompletionType = '<c-p>'

set backspace=indent,eol,start " backspace over everything in insert mode

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPBuffer' " CtrlP, CtrlPMRU, CtrlPMixed

" Make sure ctrl-v works in command mode on windows
cmap <C-V> <C-R>+


let g:jedi#force_py_version = 3    " only use Python3
let g:jedi#popup_on_dot = 0        " don't pop up list every time I type a dot
let g:jedi#smart_auto_mappings = 0 " don't do 'from module.name<space>' magic
" let g:jedi#completions_command = '<Tab>'


" Configure lightline to show ALE messages from Pylint
let g:lightline = {}

let g:lightline.component_expand = {
            \  'linter_checking': 'lightline#ale#checking',
            \  'linter_warnings': 'lightline#ale#warnings',
            \  'linter_errors': 'lightline#ale#errors',
            \  'linter_ok': 'lightline#ale#ok',
            \ }

let g:lightline.component_type = {
            \     'linter_checking': 'left',
            \     'linter_warnings': 'warning',
            \     'linter_errors': 'error',
            \     'linter_ok': 'left',
            \ }

let g:lightline.active = { 'right': [
            \ [ 'linter_checking', 'linter_warnings', 'linter_errors', 'linter_ok', ],
            \ [ 'percent', 'lineinfo', ],
            \ [ 'fileformat', 'fileencoding', 'filetype', ],
            \ ] }
" , 'charvaluehex'

" ALE configuration
" let g:ale_completion_enabled = 0
let g:ale_echo_msg_format = '[%linter%] %code: %%s'
" let g:ale_python_mypy_ignore_invalid_syntax = 1
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_text_changed = 0
let g:ale_linters = {'python' : ['mypy', 'pylint']}

nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" :h statusline
set guitablabel=%M\ %N\ %t

let g:vim_markdown_folding_disabled = 1
