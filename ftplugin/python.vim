
nmap <buffer> gx :!python %:p<CR>
nmap <buffer> gX :!start python %:p<CR>
map <buffer> <C-e> :w<CR> <bar> :!python %:p<CR>
map <buffer> <F8> :w<CR> <bar> :!start python %:p<CR>
