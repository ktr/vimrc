
from shutil import copyfile
import os

base_dir = r'C:\Program Files (x86)\Vim'

live = os.path.join(base_dir, '.vimrc')
backup = os.path.basename(live)
copyfile(live, backup)

track = (
    'ftplugin/vb.vim',
    'ftplugin/python.vim',
    'ftplugin/sql.vim',
    'ftplugin/help.vim',
    'colors/smyck.vim',
)
for filename in track:
    live = os.path.join(base_dir, 'vimfiles', filename)
    copyfile(live, filename)
